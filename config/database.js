const config = new URL(
  process.env.DATABASE_URL ||
      "postgres://postgres:kelvin123@127.0.0.1:5432/bcr"
)

const {
  DB_USER = config.username || "postgres",
  DB_PASSWORD = config.password || "27022002setiawan",
  DB_NAME = config.pathname.replace("/", "") || "binar-chapter-8",
  DB_HOST = config.hostname || "127.0.0.1",
  DB_PORT = "5432",
} = process.env

module.exports = {
  development: {
      username: DB_USER,
      password: DB_PASSWORD,
      database: DB_NAME,
      host: DB_HOST,
      port: DB_PORT,
      ssl: true,
      dialectOptions: {
          ssl: {
              require: true,
              rejectUnauthorized: false,
          },
      },
      dialect: "postgres",
  },
  test: {
      username: DB_USER,
      password: DB_PASSWORD,
      database: `${DB_NAME}_test`,
      host: DB_HOST,
      port: DB_PORT,
      // storage: "node_modules/test.sqlite",
      dialect: "postgres",
      logging: false,
  },
  production: {
      username: DB_USER,
      password: DB_PASSWORD,
      database: DB_NAME,
      host: DB_HOST,
      port: DB_PORT,
      ssl: true,
      dialectOptions: {
          ssl: {
              require: true,
              rejectUnauthorized: false,
          },
      },
      dialect: "postgres",
  },
}
