const app = require('../../../../../../app')
const { Car } = require('../../../../../../app/models')
const request = require('supertest')
const { JsonWebTokenError } = require('jsonwebtoken')
const { InsufficientAccessError } = require('../../../../../../app/errors')


describe('Delete route (DELETE /v1/cars/:id', () => {
    const carModel = Car
    describe('Successful Operation', () => {
        let token
        const userCredential = {
            email: "johnny@binar.co.id",
            password: "123456",
        }
        let newCar
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    token = res.body.accessToken
                    
                    const newCarDetail = {
                        name: "Testing 1",
                        price: 1250000,
                        size: "SMALL",
                        image: "https://source.unsplash.com/531x531"
                    }
                    newCar = await carModel.create(newCarDetail)
                    done()
                })
            })

        afterAll(() => {

        })

        it('(valid request params) should return status 204 and doesn\'t have any body', (done) => {
            request(app)
                .delete(`/v1/cars/${newCar.id}`)
                .set('Authorization', `Bearer ${token}`)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.status).toEqual(204)
                    done()
                })
        })
    })
    describe('Error Operation', () => {
        let validToken
        const validUserCredential = {
            email: "johnny@binar.co.id",
            password: "123456"
        }
        
        let invalidToken
        const invalidUserCredential = {
            email: "brian@binar.co.id",
            password: "123456",
        }

        let newCar
        beforeAll((done) => {
            request(app)
                .post('/v1/auth/login').
                send(validUserCredential)
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    validToken = res.body.accessToken

                    request(app)
                        .post('/v1/auth/login')
                        .send(invalidUserCredential)
                        .end(async (err, res) => {
                            if (err) return done(err)
        
                            expect(res.header['content-type']).toMatch(/json/)
                            expect(res.status).toEqual(201)
                            invalidToken = res.body.accessToken
                            
                            const newCarDetail = {
                                name: "Testing 1",
                                price: 1250000,
                                size: "SMALL",
                                image: "https://source.unsplash.com/531x531"
                            }
                            newCar = await carModel.create(newCarDetail)
                            done()
                        })
                })
            })

        afterAll(async () => {
            await carModel.destroy({ where: { id: newCar.id } })
        })

        it('(invalid requst id but valid request body) should be returning status code 404 and return error with message not found', (done) => {
            // const expectedError = new TypeError("Cannot read properties of null (reading 'update')")
            const expectedResponse = {
                error: {
                    details: {
                        method: "DELETE",
                        url: "/v1/cars/"
                    },
                    message: "Not found!",
                    name: "Error"
                }
            }
            request(app)
                .delete(`/v1/cars/ `)
                .set("Authorization", `Bearer ${validToken}`)
                .set('Accept', 'application/json')
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(404)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(invalid token) should be returning status code 401 and return error InsufficientAccessError', (done) => {
            const expectedError = new InsufficientAccessError("CUSTOMER")
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .delete(`/v1/cars/${newCar.id}`)
                .set("Authorization", `Bearer ${invalidToken}`)
                .set('Accept', 'application/json')
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(no authorization header) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const expectedError = new JsonWebTokenError("jwt must be provided")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .delete(`/v1/cars/${newCar.id}`)
                .set('Accept', 'application/json')
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })

        it('(no token) should be returning status code 401 and return error JsonWebTokenError', (done) => {
            const expectedError = new JsonWebTokenError("jwt malformed")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .delete(`/v1/cars/${newCar.id}`)
                .set('Authorization', `Bearer 123456`)
                .set('Accept', 'application/json')
                .end(async (err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)

                    expect(res.body).toEqual(expectedResponse)
                    done()
            })
        })
    })
})