const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('handleListCars function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })
        it('(valid request with offset, pageSize, size and availableAt) should return status code 200 and return lists of cars', async () => {
            const mReq = { query: { page: 2, pageSize: 5, size: "SMALL", availableAt: new Date(new Date().setDate(new Date(). getDate() - 100)) } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleListCars(mReq, mRes, mNext)

            
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {

    })
})
