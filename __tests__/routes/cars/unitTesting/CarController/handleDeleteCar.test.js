const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('handleDeleteCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        let car
        beforeAll(async () => {
            car = await carModel.create({
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            })            
        })

        afterAll(() => {
            
        })

        it('(valid request body and params) should return status code 204 and return the deleted car', async () => {
            const mReq = { params: { id: car.id } }
            const mRes = { status: jest.fn().mockReturnThis(), end: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleDeleteCar(mReq, mRes, mNext)
            expect(mRes.status).toBeCalledWith(204)
        })
    })

    describe('Error Operation', () => {
       
    })
})
