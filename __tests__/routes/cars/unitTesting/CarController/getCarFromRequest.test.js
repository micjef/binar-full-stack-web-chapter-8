const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('getCarFromRequest function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })

        it('(valid request params) should return one car object', async () => {
            const resultResponse = await carController.getCarFromRequest({
                params: {
                    id: 1
                }
            })

            const expectedResponse = await carModel.findByPk(1)
            expect(resultResponse).toEqual(expectedResponse)
        })
    })

    describe('Error Operation', () => {
        
    })
})
