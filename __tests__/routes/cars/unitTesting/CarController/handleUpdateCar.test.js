const { CarController } = require('../../../../../app/controllers')
const { Car, UserCar } = require('../../../../../app/models')
const dayjs = require('dayjs')

describe('handleUpdateCar function', () => {
    const carModel = Car
    const userCarModel = UserCar
    const carController = new CarController({ carModel, userCarModel, dayjs })

    describe('Successful Operation', () => {
        let car
        beforeAll(async () => {
            car = await carModel.create({
                name: "Testing 1",
                price: 1250000,
                size: "SMALL",
                image: "https://source.unsplash.com/531x531"
            })            
        })

        afterAll(async () => {
            await carModel.destroy({ where: { id: car.id } })
        })

        it('(valid request body and params) should return status code 200 and return the updated car', async () => {
            const updatedCar = {
                name: "Testing 2",
                price: 150000000,
                size: "LARGE",
                image: "https://source.unsplash.com/500x500"
            }
            const mReq = { body: updatedCar, params: { id: car.id } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleUpdateCar(mReq, mRes, mNext)
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeDefined()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(invalid params id) should return 422 and return error of specific type', async () => {
            const updatedCar = {
                name: "Testing 2",
                price: 150000000,
                size: "LARGE",
                image: "https://source.unsplash.com/500x500"
            }
            const mReq = { body: updatedCar, params: {  } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await carController.handleUpdateCar(mReq, mRes, mNext)
            
            const error = new TypeError("Cannot read properties of null (reading 'update')")
            const expectedError = {
                error: {
                    message: error.message,
                    name: error.name
                }
            }
            expect(mRes.status).toBeCalledWith(422)
            expect(mRes.json).toBeCalledWith(expectedError)
        })
    })
})
