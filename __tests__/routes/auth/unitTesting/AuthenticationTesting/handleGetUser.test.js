const { Role, User } = require('../../../../../app/models')
const { RecordNotFoundError } = require('../../../../../app/errors');
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

describe('handleGetUser function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    
    describe('Successful Operation', () => {
        let user
        let userRole
        beforeAll(async () => {
            user = await userModel.findByPk(1)
            userRole = await roleModel.findByPk(user.roleId)
        })
        
        afterAll(() => {
            
        })

        it('(valid jwt token) should return status code 200 and return the user object of corresponding jwt token payload', async () => {
            const mReq = { user: { id: user.id } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleGetUser(mReq, mRes, mNext)
            
            const expectedResponse = {
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                    id: userRole.id,
                    name: userRole.name
                }
            }
            expect(mRes.status).toBeCalledWith(200)
            expect(mRes.json).toBeCalledWith(expectedResponse)  
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })

        it('(no user found) should return status code 404 and return error with message RecordNotFoundError', async () => {
            const mReq = { user: { id: 0 } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            await authenticationController.handleGetUser(mReq, mRes, mNext)
            
            const expectedResponse = new RecordNotFoundError()
            expect(mRes.status).toBeCalledWith(404)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })
})
