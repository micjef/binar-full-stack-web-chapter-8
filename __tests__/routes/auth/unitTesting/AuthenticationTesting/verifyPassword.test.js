const { Role, User } = require('../../../../../app/models')
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

describe('verifyPassword function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })
    
    const expectedPassword = "123456"
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(valid password) should return true', async () => {
            const password = "123456"
            const hashedPassword = bcrypt.hashSync(password, 10)
            const result = authenticationController.verifyPassword(expectedPassword, hashedPassword)
            expect(result).toBeTruthy()
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {

        })

        afterAll(() => {
            
        })
        it('(blank password) should return error of specific type', async () => {
            const password = null
            const expectedError = new Error(`Illegal arguments: ${typeof password}, string`)

            const func = () => {
                try{
                    const hashedPassword = bcrypt.hashSync(password, 10)
                    authenticationController.verifyPassword(expectedPassword, hashedPassword)
                } catch(err){
                    throw err
                }
            }
            expect(func).toThrow(expectedError)
        })

        it('(invalid password) should return false', async () => {
            const password = ""
            const hashedPassword = bcrypt.hashSync(password, 10)
            const result = authenticationController.verifyPassword(expectedPassword, hashedPassword)
            expect(result).not.toBeTruthy()
        })
    })
})

