const { Role, User } = require('../../../../../app/models')
const { InsufficientAccessError } = require('../../../../../app/errors');
const AuthenticationController = require('../../../../../app/controllers/AuthenticationController')
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { JWT_SIGNATURE_KEY } = require('../../../../../config/application');

describe('authorize function', () => {
    const roleModel = Role
    const userModel = User
    const authenticationController = new AuthenticationController({ bcrypt, jwt, roleModel, userModel })

    describe('Successful Operation', () => {
        let token

        beforeAll(() => {
            token = jwt.sign({
                id: 3,
                name: "brian",
                email: "brian@binar.co.id",
                image: null,
                role: {
                    id: 1,
                    name: "CUSTOMER"
                }
            }, JWT_SIGNATURE_KEY)
        })

        afterAll(() => {

        })

        it('(valid jwt token) should hit next endpoint', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('CUSTOMER')(mReq, mRes, mNext)
            
            expect(mNext).toHaveBeenCalled()
        })
    })

    describe('Error Operation', () => {
        let token

        beforeAll(() => {
            token = jwt.sign({
                id: 3,
                name: "brian",
                email: "brian@binar.co.id",
                image: null,
                role: {
                    id: 1,
                    name: "CUSTOMER"
                }
            }, JWT_SIGNATURE_KEY)
        })

        afterAll(() => {

        })

        it('(empty token) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: 'Bearer ' } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new jwt.JsonWebTokenError("jwt must be provided")
            const expecteResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expecteResponse)
        })

        it('(invalid token token) should return status code 401 and return error JsonWebToken', () => {
            const mReq = { headers: { authorization: 'Bearer 123456' } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new jwt.JsonWebTokenError("jwt malformed")
            const expecteResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expecteResponse)
        })


        it('(invalid rolename) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize('ADMIN')(mReq, mRes, mNext)

            const expectedError = new InsufficientAccessError("CUSTOMER")
            const expectedResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: expectedError.details
                }
            }
            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })

        it('(empty parameter) should return status code 401 and return error InsufficientAccessError', () => {
            const mReq = { headers: { authorization: `Bearer ${token}` } }
            const mRes = { status: jest.fn().mockReturnThis(), json: jest.fn().mockReturnThis() }
            const mNext = jest.fn()
            authenticationController.authorize()(mReq, mRes, mNext)

            const expectedError = new Error("invalid parameter")
            const expectedResponse = {
                error: {
                    name: expectedError.name,
                    message: expectedError.message,
                    details: null
                }
            }

            expect(mRes.status).toBeCalledWith(401)
            expect(mRes.json).toBeCalledWith(expectedResponse)
        })
    })
})