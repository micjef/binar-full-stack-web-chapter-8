const app = require('../../../../../../app')
const request = require('supertest')
const { EmailAlreadyTakenError } = require('../../../../../../app/errors')
const { User } = require('../../../../../../app/models')


describe('Register route (POST /v1/register)', () => {
    describe('Successful Operation', () => {
        beforeAll(() => {

        })

        afterAll(async () => {
            await User.destroy({ 
                where: { 
                    name: "Kelvin",
                    email: "kelvin@binar.co.id",
                }
            })
        })

        it('(unregistered email) should return status code 201 and return accessToken of new registered user', (done) => {
            const newUserCredentials = {
                name: "Kelvin",
                email: "kelvin@binar.co.id",
                password: "kelvin123"
            }
            request(app)
                .post('/v1/auth/register')
                .send(newUserCredentials)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    expect(res.body.accessToken).toBeDefined()
                    done()
                })
            
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        it('(email taken) should return status code 422 and return error with error EmailAlreadyTakenError', (done) => {
            const newUserCredential = {
                name: "brian",
                email: 'brian@binar.co.id',
                password: 'binar123'
            }
            const expectedError = new EmailAlreadyTakenError(newUserCredential.email)
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }

            request(app)
                .post('/v1/auth/register')
                .send(newUserCredential)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(422)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })
        it('(empty request body) should return status code 500 and return error of specific type', (done) => {
            const expectedError = new TypeError("Cannot read properties of undefined (reading 'toLowerCase')")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .post('/v1/auth/register')
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(500)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        
    })
})