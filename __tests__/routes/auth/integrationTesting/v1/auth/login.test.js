const app = require('../../../../../../app')
const request = require('supertest')
const { EmailNotRegisteredError, WrongPasswordError } = require('../../../../../../app/errors')


describe('Login route (POST /v1/login)', () => {
    describe('Successful Operation', () => {
        beforeAll(() => { 
            
         })
         
        afterAll(() => { 
            
         })
        it('(valid email and password) should return status code 201 and return accessToken of logged in user', (done) => {
            const userCredential = {
                email: "johnny@binar.co.id",
                password: "123456",
            }
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(201)
                    expect(res.body.accessToken).toBeDefined()
                    done()
                })
        })
    })

    describe('Error Operation', () => {
        beforeAll(() => {
            
        })
        
        afterAll(() => {
            
        })
        it('(not registered email) should return status code 404 and return error EmailNotRegisteredError', (done) => {
            const userCredential = {
                email: "testing@binar.com",
                password: "testing123"
            }
            const expectedError = new EmailNotRegisteredError(userCredential.email)
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(404)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        it('(wrong password) should return status code 401 and return error WrongPasswordError', (done) => {
            const userCredential = {
                email: "brian@binar.co.id",
                password: "testing123"
            }
            const expectedError = new WrongPasswordError()
            const expectedResponse = {
                error: {
                    details: expectedError.details,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .post('/v1/auth/login')
                .send(userCredential)
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(401)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })

        it('(empty request body) should return status code 500 and return error of specific type', (done) => {
            const expectedError = new TypeError("Cannot read properties of undefined (reading 'toLowerCase')")
            const expectedResponse = {
                error: {
                    details: null,
                    message: expectedError.message,
                    name: expectedError.name
                }
            }
            request(app)
                .post('/v1/auth/login')
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(500)
                    expect(res.body).toEqual(expectedResponse)
                    done()
                })
        })
    })
})