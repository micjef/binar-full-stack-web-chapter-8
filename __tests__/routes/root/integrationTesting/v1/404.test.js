const app = require('../../../../../app')
const request = require('supertest')
const { NotFoundError } = require('../../../../../app/errors')

describe('404 route (GET /hello)', () => {
    describe('Successful Operation', () => {
        beforeAll(() => { 
            
         })
         
        afterAll(() => { 
            
         })
        it('(valid request) should return status code 404 and return error NotFoundError', (done) => {
            request(app)
                .get('/hello')
                .set('Accept', 'application/json')
                .end((err, res) => {
                    if (err) return done(err)

                    expect(res.header['content-type']).toMatch(/json/)
                    expect(res.status).toEqual(404)
                    
                    const expectedError = new NotFoundError("GET", "/hello")
                    expect(res.body.error.name).toEqual(expectedError.name)
                    expect(res.body.error.message).toEqual(expectedError.message)
                    expect(res.body.error.details).toEqual(expectedError.details)

                    done()
                })
        })
    })

    describe('Error Operation', () => {
        
    })
})