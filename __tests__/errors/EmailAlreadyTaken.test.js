const EmailAlreadyTaken = require('../../app/errors/EmailAlreadyTaken')
const { User } = require('../../app/models')

describe('EmailAlreadyTaken Class', () => {
    const userModel = User
    describe('Successful create', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('should get details json object', async () => {
            const user = await userModel.findByPk(1)
            const emailAlreadyTaken = new EmailAlreadyTaken(user.email)

            const expectedResult = {
                email: user.email
            }
            expect(emailAlreadyTaken.details).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {

    })
})