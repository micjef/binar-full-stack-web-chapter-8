const ApplicationError = require('../../app/errors/ApplicationError')

describe('ApplicationError Class', () => {
    describe('Successful create', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('should get empty json object', () => {
            const applicationError = new ApplicationError()

            expect(applicationError.details).toEqual({})
        })

        it('should get error json object', () => {
            const applicationError = new ApplicationError()
            
            const expectedResult = {
                error: {
                    name: applicationError.name,
                    message: applicationError.message,
                    details: applicationError.details
                }
            }
            expect(applicationError.toJSON()).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {

    })
})