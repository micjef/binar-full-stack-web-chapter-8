const EmailNotRegisteredError = require('../../app/errors/EmailNotRegisteredError')

describe('EmailAlreadyTaken Class', () => {
    describe('Successful create', () => {
        beforeAll(() => {

        })

        afterAll(() => {

        })

        it('should get details json object', async () => {
            const email = "kelvin@binar.co.id"
            const emailNotRegisteredError = new EmailNotRegisteredError(email)

            const expectedResult = {
                email
            }
            expect(emailNotRegisteredError.details).toEqual(expectedResult)
        })
    })
    describe('Error create', () => {

    })
})